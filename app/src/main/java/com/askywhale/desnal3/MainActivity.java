package com.askywhale.desnal3;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.github.panpf.zoomimage.SketchZoomImageView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MainActivity extends Activity implements View.OnLongClickListener {

    private SketchZoomImageView view;
    private Handler uiHandler;
    ExecutorService executor;
    private int prcDone;
    private Bitmap bmp;
    private boolean shouldStop;
    private static final Map<Integer, Bitmap> cache = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        uiHandler = new Handler(Looper.getMainLooper());
        executor = Executors.newFixedThreadPool(2);
        view = findViewById(R.id.sziv_image);
        view.setBackgroundColor(Color.parseColor("#000000"));
        view.setOnLongClickListener(this);
        redraw();
    }

    @Override
    public boolean onLongClick(View view) {
        shouldStop = true;
        startActivityForResult(new Intent(this, ConfigActivity.class), 0);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        redraw();
    }

    private void redraw() {
        ProgressBar pgWait = findViewById(R.id.pg_wait);
        pgWait.setVisibility(View.VISIBLE);
        shouldStop = false;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                doRedrawExp4j();
            }
        });
    }

    private void doRedrawExp4j() {
        Log.d("MainActivity", "enter redraw() - exp4j");
        SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
        String hue = prefs.getString("hue", ConfigActivity.DEFAULT_HUE);
        String saturation = prefs.getString("saturation", ConfigActivity.DEFAULT_SATURATION);
        String lightness = prefs.getString("lightness", ConfigActivity.DEFAULT_LIGHTNESS);
        int width = prefs.getInt("width", ConfigActivity.DEFAULT_WIDTH);
        int height = prefs.getInt("height", ConfigActivity.DEFAULT_HEIGHT);
        int hash = prefs.getInt("hash", 0);
        try {
            Expression[] expressions = new Expression[]{
                    new ExpressionBuilder(hue).variables("x", "y", "X", "Y", "a", "d", "D", "w", "h").build(),
                    new ExpressionBuilder(saturation).variables("x", "y", "X", "Y", "a", "d", "D", "w", "h").build(),
                    new ExpressionBuilder(lightness).variables("x", "y", "X", "Y", "a", "d", "D", "w", "h").build()
            };
            for (int i = 0; i < 3; i++)
                expressions[i] = expressions[i].setVariable("w", width).setVariable("h", height);
            bmp = null;
            if(cache.containsKey(hash))
                bmp = cache.get(hash);
            else {
                bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                for (int x = 0; x < width && !shouldStop; x++) {
                    for (int y = 0; y < height && !shouldStop; y++) {
                        float[] hsv = new float[3];
                        for (int i = 0; i < 3; i++) {
                            double dist = Math.sqrt((x - width / 2) * (x - width / 2) + (y - height / 2) * (y - height / 2));
                            hsv[i] = (float)expressions[i].
                                    setVariable("x", ((double)x)/width-0.5).
                                    setVariable("y", ((double)y)/height-0.5).
                                    setVariable("X", x-width/2).
                                    setVariable("Y", y-height/2).
                                    setVariable("a", Math.atan2(y-height/2, x-width/2)).
                                    setVariable("d", dist /
                                            Math.sqrt(width*width+height*height)).
                                    setVariable("D", dist).
                                    evaluate();
                        }
                        bmp.setPixel(x, y, Color.HSVToColor(new float[]{hsv[0] * 360, hsv[1], hsv[2]}));
                    }
                    prcDone = 100 * x / width;
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ProgressBar pgWait = findViewById(R.id.pg_wait);
                            pgWait.setProgress(prcDone);
                        }
                    });
                }
                Date now = new Date();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss", Locale.getDefault());
                saveBitmap(this, bmp, Bitmap.CompressFormat.PNG, "image/png", "desnal3", df.format(now));
                cache.put(hash, bmp);
            }
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    view.setImageBitmap(bmp);
                }
            });
        } catch (Exception ex) {
            Log.i("MainActivity", "Exception", ex);
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

        Log.d("MainActivity", "done redraw()");
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                ProgressBar pgWait = findViewById(R.id.pg_wait);
                pgWait.setVisibility(View.INVISIBLE);
            }
        });
    }

    @NonNull
    public Uri saveBitmap(@NonNull final Context context, @NonNull final Bitmap bitmap,
                          @NonNull final Bitmap.CompressFormat format,
                          @NonNull final String mimeType,
                          @NonNull final String folder,
                          @NonNull final String displayName) {

        final ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DISPLAY_NAME, displayName);
        values.put(MediaStore.MediaColumns.MIME_TYPE, mimeType);
        values.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM + "/" + folder);

        final ContentResolver resolver = context.getContentResolver();
        Uri uri = null;

        try {
            final Uri contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            uri = resolver.insert(contentUri, values);

            if (uri == null)
                throw new IOException("Failed to create new MediaStore record.");

            try (final OutputStream stream = resolver.openOutputStream(uri)) {
                if (stream == null)
                    throw new IOException("Failed to open output stream.");

                if (!bitmap.compress(format, 95, stream))
                    throw new IOException("Failed to save bitmap.");
            }

            return uri;
        }
        catch (IOException ex) {
            if (uri != null) {
                resolver.delete(uri, null, null);
            }
            Log.e("MainActivity", "Exception saving bitmap", ex);
            return null;
        }
    }
/*
// EvalEx - bigdecimal - too slow
    private void doRedrawEvalEx() {
        Log.d("MainActivity", "enter redraw() - evalEx");
        Config config = ConfigActivity.config;
        Expression[] expressions = new Expression[]{
                new Expression(config.hue),
                new Expression(config.saturation),
                new Expression(config.lightness)
        };
        for (int i = 0; i < 3; i++)
            expressions[i] = expressions[i].with("w", config.width).and("h", config.height);
        try {
            bmp = null;
            if(cache.containsKey(config.hashCode()))
                bmp = cache.get(config.hashCode());
            else {
                bmp = Bitmap.createBitmap(config.width, config.height, Bitmap.Config.ARGB_8888);
                for (int x = 0; x < config.width && !shouldStop; x++) {
                    for (int y = 0; y < config.height && !shouldStop; y++) {
                        float[] hsv = new float[3];
                        for (int i = 0; i < 3; i++) {
                            hsv[i] = expressions[i].with("x", x).and("y", y).evaluate().getNumberValue().floatValue();
                        }
                        bmp.setPixel(x, y, Color.HSVToColor(new float[]{hsv[0] * 360, hsv[1], hsv[2]}));
                    }
                    prcDone = 100 * x / config.width;
                    uiHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ProgressBar pgWait = findViewById(R.id.pg_wait);
                            pgWait.setProgress(prcDone);
                        }
                    });
                }
                cache.put(config.hashCode(), bmp);
            }
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    view.setImageBitmap(bmp);
                }
            });
        } catch (ParseException | EvaluationException ex) {
            Log.i("MainActivity", "Exception", ex);
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        Log.d("MainActivity", "done redraw()");
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                ProgressBar pgWait = findViewById(R.id.pg_wait);
                pgWait.setVisibility(View.INVISIBLE);
            }
        });

    }
*/
}
