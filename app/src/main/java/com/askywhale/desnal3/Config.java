package com.askywhale.desnal3;

import java.util.Objects;

public class Config {
    public String hue, saturation, lightness;
    public int width, height;

    public Config() {
        hue = "0";
        saturation = "1";
        lightness = "1";
        width = 2048;
        height = 1152;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Config config = (Config) o;
        return width == config.width && height == config.height && hue.equals(config.hue) && saturation.equals(config.saturation) && lightness.equals(config.lightness);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hue, saturation, lightness, width, height);
    }
}
