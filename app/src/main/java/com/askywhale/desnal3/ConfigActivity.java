package com.askywhale.desnal3;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import java.util.Objects;

public class ConfigActivity extends Activity implements View.OnClickListener {

    public static final String DEFAULT_HUE = "x/w";
    public static final String DEFAULT_SATURATION = "y/h";
    public static final String DEFAULT_LIGHTNESS = "1";
    public static int DEFAULT_WIDTH = 1024;
    public static int DEFAULT_HEIGHT = 576;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config);
        SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
        ((EditText)findViewById(R.id.et_hue)).setText(prefs.getString("hue", DEFAULT_HUE));
        ((EditText)findViewById(R.id.et_saturation)).setText(prefs.getString("saturation", DEFAULT_SATURATION));
        ((EditText)findViewById(R.id.et_lightness)).setText(prefs.getString("lightness", DEFAULT_LIGHTNESS));
        ((EditText)findViewById(R.id.et_width)).setText(String.valueOf(prefs.getInt("width", DEFAULT_WIDTH)));
        ((EditText)findViewById(R.id.et_height)).setText(String.valueOf(prefs.getInt("height", DEFAULT_HEIGHT)));
        Button btOk = findViewById(R.id.bt_ok);
        btOk.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        SharedPreferences prefs = getSharedPreferences("config", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String hue = ((EditText)findViewById(R.id.et_hue)).getText().toString();
        String saturation = ((EditText)findViewById(R.id.et_saturation)).getText().toString();
        String lightness = ((EditText)findViewById(R.id.et_lightness)).getText().toString();
        int width = Integer.parseInt(((EditText)findViewById(R.id.et_width)).getText().toString());
        int height = Integer.parseInt(((EditText)findViewById(R.id.et_height)).getText().toString());
        editor.putString("hue", hue);
        editor.putString("saturation", saturation);
        editor.putString("lightness", lightness);
        editor.putInt("width", width);
        editor.putInt("height", height);
        editor.putInt("hash", Objects.hash(hue, saturation, lightness, width, height));
        editor.commit();
        setResult(0);
        finish();
    }

}
