package com.askywhale.desnal3;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ConfigTest {

	@Test
	public void testHashCode() {
		Config c1 = new Config();
		Config c2 = new Config();
		assertEquals(c1.hashCode(), c2.hashCode());
	}

}
